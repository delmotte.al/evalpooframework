<?php
// ce service devrait permettre d'importer les donnée de la db vers le projet
// src/Service/ImportService.php
namespace App\Service;

use Doctrine\DBAL\Connection;

class ImportService
{
    private $externalDatabaseConnection;

    public function __construct(Connection $externalDatabaseConnection)
    {
        $this->externalDatabaseConnection = $externalDatabaseConnection;
    }

    public function importData()
    {
        $query = 'SELECT * FROM course';
        $data = $this->externalDatabaseConnection->fetchAll($query);

        // Traitement des données récupérées et sauvegarde dans votre entité Symfony
        // ...
    }
}
