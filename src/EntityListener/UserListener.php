<?php
namespace App\EntityListener;


use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Attribute\AsEntityListener;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Doctrine\ORM\Events;
use Doctrine\ORM\Mapping as ORM;


class UserListener
{
    private  UserPasswordHasherInterface $hasher ;

    public function __construct( UserPasswordHasherInterface $hasher){
        $this->hasher = $hasher;
    }
    public function prePersist(User $User): void
    {
        $this->encodePassword($User);
    }
    public function preUpdate(User $User)
    {
        $this->encodePassword($User);
    }

    /*
     * Encode Password based on plain password
     */
    public function encodePassword(User $User)
    {
        if($User->getPlainPassword()=== null)
        {
            return;
        }
        $User->setPassword(
            $this->hasher->hashPassword(
                $User,
                $User->getPlainPassword())
        );
        $User->setPlainPassword(null);
    }
}