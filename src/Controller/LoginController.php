<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class LoginController extends AbstractController
{
    #[Route('/login', name: 'templates.login' , methods:['GET' , 'POST'])]
    public function login(AuthenticationUtils $autenticationUtils): Response
    {
        return $this->render('login.html.twig', [
            'last_username' => $autenticationUtils->getLastUsername(),
            'error' => $autenticationUtils->getLastAuthenticationError()
        ]);
    }
    #[Route('/logout', name: 'templates.logout' )]
    public function logout(){
        // il fait tout tout seul
    }
}
