<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    #[Route('/' , 'home.home', methods:['GET'])]
    public function home(){
        $name = 'Alex';
      return $this->render('home.html.twig' , ['home' =>'hello ' . $name . ' ! :-) <3']);
    }



}
