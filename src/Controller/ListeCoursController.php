<?php

namespace App\Controller;

use App\Repository\CourseRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ListeCoursController extends AbstractController
{
    #[Route('/listeCours', name: 'app_liste_cours' , methods:['GET'])]
    public function index(CourseRepository $repository): Response
    {
        $course = $repository->findAll();

        return $this->render('listeCours.html.twig', [
            'course' => $course
        ]);
    }
}
