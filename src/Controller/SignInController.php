<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class SignInController extends AbstractController
{
    #[Route('/signIn', name: 'signIn.index', methods: ['GET'])]
    public function index(): Response
    {
        return $this->render('signIn.html.twig', [
            'signIn' => 'SignInController',
        ]);
    }
}
