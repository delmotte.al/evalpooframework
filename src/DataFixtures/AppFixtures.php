<?php

namespace App\DataFixtures;

use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Validator\Constraints\DateTime;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

use App\Entity\Course;
use App\Entity\User;


class AppFixtures extends Fixture
{

    public function load(ObjectManager $manager): void
    {
        //courses
        $code = 1000 ;
       for($i = 0 ; $i < 20; $i++) {
           $Course = new Course();
           $Course->setName('Course ' . $i);
           $Course->setCode($code += 10);
           $manager->persist($Course);
       }

       // User
        for($i = 0 ; $i < 20; $i++) {
        $User = new User();
        $User->setName('toto' . $i);
        $User->setRoles(['visiteur']);
        $User->setEmail('toto' . $i . '@gmail.com');
        $User->setLastlogin(null);
        $User->setPlainPassword('Pass_1234');

           $manager->persist( $User);

       }


        $manager->flush();




    }


}
